from django.db import models


class Questions(models.Model):

    quest = models.CharField(max_length=200, verbose_name=u'Pytanie')
    answer = models.CharField(max_length=200, verbose_name=u'Odpowiedź')
