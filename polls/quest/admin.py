from django.contrib import admin
from .models import Questions


class QuestionsAdmin(admin.ModelAdmin):
    list_display = ('quest', 'answer',)


admin.site.register(Questions, QuestionsAdmin)
admin.site.site_header = u'Ankiety'
