from django.shortcuts import render
from .models import Questions
from django.views.generic import ListView, DetailView


class QuestListView(ListView):
    model = Questions

    def get_queryset(self):
        return Questions.objects.all()


class QuestDetailView(DetailView):
    model = Questions

    def get_queryset(self):
        return Questions.objects.all()
