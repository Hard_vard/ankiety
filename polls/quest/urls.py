from django.conf.urls import url

from .views import QuestListView, QuestDetailView

urlpatterns = [
    url(r'^$', QuestListView.as_view(), name='quests'),
    url(r'^(?P<pk>/d)/$', QuestDetailView.as_view(), name='answer'),
]
