# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Questions',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('answer', models.CharField(max_length='200', verbose_name='Odpowiedź')),
            ],
            options={
                'verbose_name_plural': 'Pytania',
                'verbose_name': ('Pytanie',),
            },
        ),
    ]
